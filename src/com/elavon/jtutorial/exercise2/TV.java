package com.elavon.jtutorial.exercise2;

public class TV {

	protected String brand;
	protected String model;
	protected Boolean powerOn = false;
	protected Integer channel = 0;
	protected Integer volume = 5;

	TV(String brand, String model){
		this.brand = brand;
		this.model = model;
	}
	
	public static void main(String[] args) {
		TV tv1 = new TV("Sony", "Alpha");
		tv1.powerOn = true;
		tv1.channel = 0;
		tv1.volume = 5;
		System.out.println(tv1);
				
		TV tv2 = new TV("Andre Electronics","ONE");
		System.out.println(tv2);
		
		tv2.channelUp();
		tv2.channelUp();
		tv2.channelUp();
		tv2.channelUp();
		tv2.channelUp();
		tv2.channelDown();
		tv2.volumeDown();
		tv2.volumeDown();
		tv2.volumeDown();
		tv2.volumeUp();
		tv2.turnOff();
		
		System.out.println(tv2);
	}
	
	public Boolean turnOn() {
		 return this.powerOn=true;
	}
	public Boolean turnOff() {		
		 return this.powerOn=false;
	}
	public Integer channelUp(){
		return this.channel = channel+1;		
	}

	public Integer channelDown(){
		return this.channel = channel-1;		
	}

	public Integer volumeUp(){
		return this.volume = volume+1;		
	}
	public Integer volumeDown(){
		return this.volume = volume-1;		
	}
	
	public String toString(){
	return this.brand +" "+ this.model + " [on:"+ this.powerOn + ", channel:" + this.channel +", volume:" + this.volume + "]";
	}
	
	
}
