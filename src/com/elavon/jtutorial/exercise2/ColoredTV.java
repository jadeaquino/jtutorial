package com.elavon.jtutorial.exercise2;

public class ColoredTV extends TV{

	protected Integer brightness=50;
	protected Integer contrast=50;
	protected Integer picture=50;

	public ColoredTV(String brand, String model) {
		super(brand, model);
	}
	public static void main(String[] args) {
		TV sharp = new ColoredTV("Sharp","C1");
		System.out.println(sharp);
		
		TV sonyTV,bnwTV;
		bnwTV = new TV("Admiral","A1");
		sonyTV = new ColoredTV("SONY","S1");
		System.out.println(bnwTV);
		System.out.println(sonyTV);	
		// Output of TV object reflects how TV functions 
		// while the ColoredTV reflects the added functions plus the TV basic functions
		// sonyTV.brightnessUP(); cannot be used since the sonyTV is declared as TV Object,
		// it can use the default values of ColoredTV but it can't used the methods of ColoredTV
		
		ColoredTV sharpTV = new ColoredTV("SHARP","SH1");
		sharpTV.mute();
		sharpTV.brightnessUp();
		sharpTV.brightnessUp();
		sharpTV.contrastDown();
		sharpTV.pictureUp();
		sharpTV.switchToChannel(7);
		System.out.println(sharpTV);
		
	}
	protected Integer brightnessUp(){
		return this.brightness = brightness+1;
	}
	protected Integer brightnessDown(){
		return this.brightness = brightness-1;
	}
	protected Integer contrastUp(){
		return this.contrast = contrast+1;
	}
	protected Integer contrastDown(){
		return this.contrast = contrast-1;
	}
	protected Integer pictureUp(){
		return this.picture = picture+1;
	}
	protected Integer pictureDown(){
		return this.picture = picture-1;
	}	
	protected Integer mute(){
		return this.volume =0;
	}
	protected Integer switchToChannel(int channel){
		return this.channel = channel;
	}
		
	public String toString(){
		return this.brand +" "+ this.model + " [on:"+ this.powerOn + ", channel:" + this.channel +", volume:" + this.volume + "] "
				+ "[b:"+ this.brightness + ", c:" + this.contrast +", p:" + this.picture + "]"; 
	}
	
}
